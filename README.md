# notion-app
Linux notion.so app <br>
Currently this is a work in progress. At this time it allows you to run an electron app of Notion.so on your desktop.
* A script has been provided to create the "notion.desktop" file run `./create_desktop_file.sh`.
* To access notion as a native app you will want to place the "notion.desktop" file in your 
`.local/share/applications` folder the script does not do this for you as of yet.
* If you are running arch or have issues launching the notion-app make sure you have `gconf` installed.
<br>
Thank you for trying it out.
